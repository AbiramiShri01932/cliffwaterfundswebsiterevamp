
export interface PortfolioDebtPartners {
	partner: String
	logo: String
	debtprofcount: String
	debtproftitle: String
	totaldebtscount: String
	symbol: String
	totaldebtstitle: String
	investsince: String
	investsincetitle:String
	order:String
}