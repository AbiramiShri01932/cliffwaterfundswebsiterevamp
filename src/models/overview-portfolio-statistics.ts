
export interface PortfolioStatistics {
	counts: String
	symbol: String
	details: String
}