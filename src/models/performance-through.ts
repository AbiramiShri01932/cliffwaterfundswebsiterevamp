
export interface PerformanceThrough {
	title: String
	sup: String
	annualizedreturns: String
	risk: String
	sharpe: String
	stockbeta: String
}