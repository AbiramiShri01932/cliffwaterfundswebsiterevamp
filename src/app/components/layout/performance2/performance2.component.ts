import { Component, OnInit,ViewChild,ElementRef  } from '@angular/core';
// import { Workbook } from 'exceljs';
// import * as fs from 'file-saver';
import * as XLSX from 'xlsx';
import * as Highcharts from 'highcharts';

import * as performancethrough from 'src/assets/v3/data/performancethrough.json';
import * as monthlyReturnsData from 'src/data/MonthlyReturns.json';
import * as returnHighlightsChartData from 'src/data/ReturnHighlightsChart.json';
import { PerformanceThrough } from 'src/models/performance-through';

interface MonthlyReturns {
	jan: String
	feb: String
	mar: String
	apr: String
	may: String
	jun: String
	jul: String
	aug: String
	sep: String
	oct: String
	nov: String
	dec: String
	year: String
	historyYear: Number
}

@Component({
  selector: 'app-performance2',
  templateUrl: './performance2.component.html',
  styleUrls: ['./performance2.component.scss']
})

export class Performance2Component implements OnInit {
	@ViewChild('MonthlyReturnsTable',{static:false}) table: ElementRef;
	monthlyReturns: MonthlyReturns[] = (monthlyReturnsData as any).default;
	performance:PerformanceThrough[] =(performancethrough as any).default;
	returnHighlights:any []=(returnHighlightsChartData as any).default;

	public options: any = {
			chart: {
				type: 'column'
			},
			title: {
				text: ''
			},
			exporting: { enabled: false },
			xAxis: {
				categories: this.returnHighlights[0].xAxiscategories,
				labels: {
			useHTML : true,
			title: {
				enabled: false
			},
			style:{fontSize:'22px','color':'#606771','fontFamily':'verlag-light'}
			},
			lineColor:'#29A9E4',
			plotLines: [{
			color: '#D4DAE8',
			width: 1,
			value: 0.5
				}]
			},
			yAxis: {
				title:'',
					labels: {
						formatter: function() {
							return this.value + '%';
						},
						style:{fontSize:'19px','fontFamily':'verlag-light'}
					},
					tickAmount: 4
				},
			legend: {
			enabled:true,
			useHTML:true,
			   itemStyle:{letterSpacing:'0.2em',fontSize:'13px','color':'#606771','textTransform':'uppercase','fontFamily':'verlag-light'}
	  },
	  series:this.returnHighlights
	}

	constructor() {}
  inViewClass: string;

	ngOnInit() {
		console.log(this.returnHighlights[0].xAxiscategories);
		Highcharts.chart('returnHighlightsChart', this.options);
    this.inViewClass = "in-view";

	}

	monthlyRetunrsExport(){
		const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);

		/* new format */
		var fmt = "0.00%";
		var range = { s: {r:0, c:1}, e: {r:3, c:13} };

		for(var R = range.s.r; R <= range.e.r; ++R) {
			for(var C = range.s.c; C <= range.e.c; ++C) {
				var cell = ws[XLSX.utils.encode_cell({r:R,c:C})];
				if(!cell || cell.t != 'n') continue; // only format numeric cells
				cell.z = fmt;
			}
		}

		const wb: XLSX.WorkBook = XLSX.utils.book_new();
		XLSX.utils.book_append_sheet(wb, ws, 'Monthly Returns');

		/* save to file */
		XLSX.writeFile(wb, 'Monthly Returns.xlsx');
		// let workbook = new Workbook();
		// let worksheet = workbook.addWorksheet('Monthly Returns');

		// //Add Header Row
		// let headerRow = worksheet.addRow(["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","Year"]);

		// // Cell Style : Fill and Border
		// headerRow.eachCell((cell, number) => {
		// 	cell.alignment = { vertical: 'middle', horizontal: 'center' };
		// 	cell.font = {size: 10,underline: 'single', bold: true };
		// });

		// // Add Data and Conditional Formatting
		// monthlyReturnsData.forEach(d => {
		// 	let row = worksheet.addRow([d.historyYear, d.jan, d.feb, d.mar, d.apr, d.may, d.jun, d.jul, d.aug, d.sep, d.oct, d.nov, d.dec, d.year]);
		// 	row.alignment = { vertical: 'middle', horizontal: 'center' };
		// });

		// workbook.xlsx.writeBuffer().then((data) => {
		// 	let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
		// 	fs.saveAs(blob, 'Monthly Returns.xlsx');
    	// 	})
	}
}
