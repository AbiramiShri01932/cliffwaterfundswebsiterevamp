import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Performance2Component } from './performance2.component';

describe('Performance2Component', () => {
  let component: Performance2Component;
  let fixture: ComponentFixture<Performance2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Performance2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Performance2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
