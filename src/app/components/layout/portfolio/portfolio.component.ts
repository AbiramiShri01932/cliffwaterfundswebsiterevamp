import { Component, OnInit } from '@angular/core';
import { PortfolioDebtPartners } from 'src/models/portfolio-debt-partners';
import * as portfolioDebtPartners from 'src/assets/v3/data/portfolio-debt-partners.json';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  portfolioDebtPartnersList:PortfolioDebtPartners[]=(portfolioDebtPartners as any).default;
  constructor() {
  }
  inViewClass: string;
  ngOnInit() {
    this.inViewClass = "in-view";

  }

}
