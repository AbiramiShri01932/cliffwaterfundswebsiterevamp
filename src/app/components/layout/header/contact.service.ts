import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContactService {
  constructor(private http: HttpClient) { }
  public submitContactUsForm(contact: any) {
    return this.http.post('/rest/register', contact);
  }
}