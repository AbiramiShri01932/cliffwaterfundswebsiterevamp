import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactService } from './contact.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  contactUsForm : FormGroup;
  showRespSec: boolean = false;
  statusMessage : string = "";

  constructor(
    private service: ContactService,
    private formBuilder: FormBuilder) {
    this.initContactUsForm();
   }

  ngOnInit() {
  }

  /************Init Replay Webinar Form**************/
  initContactUsForm() {
    this.contactUsForm = this.formBuilder.group({
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required, Validators.email]],
      email: ['', [Validators.required]],
      company: ['', [Validators.required, Validators.email]],
      website: ['', [Validators.required]],
      message: ['', [Validators.required]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.contactUsForm.controls; }

  /******************Method to Submit Replay Webinar Form*************** */
  public submitContactUsForm() {
    console.log('Form Value', { value: this.contactUsForm.value });
    this.service.submitContactUsForm(this.contactUsForm.value).subscribe((x: any) => {
      this.showRespSec = true;
      if (x.response === 'success') {
        this.statusMessage = "Thank you.  A Cliffwater representative will contact you shortly.";
      } else {
        this.statusMessage = "Failed to submit the form. Please try again later.";
      }
    }, (errorResponse) => {
      this.showRespSec = true;
      this.statusMessage = errorResponse.message;
    });
  }
}
