import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-literature',
  templateUrl: './literature.component.html',
  styleUrls: ['./literature.component.scss']
})
export class LiteratureComponent implements OnInit {

  constructor() { }

  inViewClass: string;
  ngOnInit() {
    this.inViewClass = "in-view";

  }
}
