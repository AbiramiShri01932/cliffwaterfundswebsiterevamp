import { Component, OnInit } from '@angular/core';
import { PortfolioStatistics } from 'src/models/overview-portfolio-statistics';
import * as portfolioStatistics from 'src/assets/v3/data/overview-portfolio-statistics.json';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  portfolioStatisticsList:PortfolioStatistics[]=(portfolioStatistics as any).default;
  constructor() { }

  ngOnInit() {
  }

}
