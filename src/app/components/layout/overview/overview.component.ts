import { Component, OnInit } from '@angular/core';

import * as keyPortfolioStatsData from 'src/data/KeyPortfolioStats.json';

interface KeyPortfolioStats {
	netcurrentYieldTxt: String
	netcurrentYieldVal: String
	floatingRateAssetTxt: String
	floatingRateAssetVal: String
	firstLienLoanTxt: String
	firstLienLoanVal: String
	underlyingCreditsTxt: String
	underlyingCreditsVal: String
	marketLendingTxt: String
	marketLendingVal: String
	totalPortfolioAssetTxt: String
	totalPortfolioAssetVal: String
}

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
	keyPortfolioStats: KeyPortfolioStats[] = (keyPortfolioStatsData as any).default;

  constructor() { }
  inViewClass: string;
  ngOnInit() {
    this.inViewClass = "in-view";

  }


}
