import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { TheaterComponent } from './components/layout/theater/theater.component';
import { NavComponent } from './components/layout/nav/nav.component';
import { OverviewComponent } from './components/layout/overview/overview.component';
import { PerformanceComponent } from './components/layout/performance/performance.component';
import { Performance2Component } from './components/layout/performance2/performance2.component';
import { PortfolioComponent } from './components/layout/portfolio/portfolio.component';
import { FactsComponent } from './components/layout/facts/facts.component';
import { LiteratureComponent } from './components/layout/literature/literature.component';
import { DebtComponent } from './components/layout/debt/debt.component';
import { InvestComponent } from './components/layout/invest/invest.component';
import { FootnotesComponent } from './components/layout/footnotes/footnotes.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { TncComponent } from './components/layout/tnc/tnc.component';
import { HomeComponent } from './components/layout/home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ContactService } from './components/layout/header/contact.service';

const appRoutes: Routes = [
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TheaterComponent,
    NavComponent,
    OverviewComponent,
    PerformanceComponent,
    Performance2Component,
    PortfolioComponent,
    FactsComponent,
    LiteratureComponent,
    DebtComponent,
    InvestComponent,
    FootnotesComponent,
    FooterComponent,
    TncComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ContactService],
  bootstrap: [AppComponent]
})
export class AppModule { }
