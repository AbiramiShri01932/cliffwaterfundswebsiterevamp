import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';


import { TncComponent } from './components/layout/tnc/tnc.component';
import { HomeComponent } from './components/layout/home/home.component';

const routes: Routes = [
  { path: 'CCLF', component: HomeComponent },
  { path: 'tnc', component: TncComponent },

  { path: "", redirectTo: '/CCLF', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
