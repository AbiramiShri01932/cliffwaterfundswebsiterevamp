# Copyright 2013 Google, Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
import json, sys
from google.appengine.api import mail
from urllib import urlencode, quote
from urllib2 import Request
from urllib2 import URLError, HTTPError
from urllib2 import urlopen
import webapp2
from mailer import Mailer

class RestHandler(webapp2.RequestHandler):

    def dispatch(self):
        # time.sleep(1)
        super(RestHandler, self).dispatch()

    def SendJson(self, r):
        self.response.headers['content-type'] = 'text/plain'
        self.response.write(json.dumps(r))

class RegisterHandler(RestHandler):

    def post(self):
        sender = Mailer()
        r = json.loads(self.request.body)
        request = self.request
        remoteip = request.remote_addr
        fname = r['fname']
        lname = r['lname']
        name=fname +' '+ lname
        email = r['email']
        company = r['company']
        website = r['website']
        message = r['message']
        print('Sending mail')
        print(self.request.body)
        sender.send_mail(name, email,company,website,message)
        sys.stdout.flush()
        self.SendJson({'response': 'success'})

APP = webapp2.WSGIApplication([
    ('/rest/register', RegisterHandler)
], debug=True)