import urllib
import json
from google.appengine.api import urlfetch

class Mailer():
    def send_mail(self, name,email,company,website,message):
        try:
            print('sending mail with deadline')
            headers = {'Content-Type': 'application/json'}
            params = {'name':name,'email':email}
            result = urlfetch.fetch(
                # url='https://cw-analytics.azurewebsites.net/api/CWFundsFactRequest?code=s5I8NI33n6zEc0gakaSrSDxPW635S2NXOzvLmsHBrr4w5g0WLk8e/A==',
                url='https://cw-analytics.azurewebsites.net/api/CWFundsFactRequestTest?code=qrbaVSrBZbor8F8SRIulI2Kah2V2TBv/D8Phnei8jY4hlWR80bm/XQ==',
                payload=json.dumps(params),
                method=urlfetch.POST,
                deadline=60,
                headers=headers)
            print(result.content)
        except urlfetch.Error:
            print('Caught exception fetching url')
        except Exception as e:
            print ("Custom Error: ---------------------->", e)